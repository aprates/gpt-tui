# GPT-TUI

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

GPT-TUI is a terminal-based user interface for GPT, written in FatScript. It offers a command line interface (CLI) for interacting with OpenAI's GPT models, enabling you to generate natural language responses without a browser.

Key features include an interactive mode that uses recent messages from the session to provide context to the GPT model, resulting in more accurate responses.

![Screen capture: GPT-TUI in action](screen-capture.gif)

## Usage

To use GPT-TUI, first obtain an API key for the OpenAI GPT model from the [OpenAI website](https://platform.openai.com/overview). Upon running the application for the first time, you will be prompted to enter your API key, which will be saved for future use.

### CLI mode

Run `gpt <prompt>` with your desired prompt as an argument. For example: `gpt What is the meaning of life?`

> CLI mode does not use context from previous messages

### Interactive mode

Enter `gpt` to initiate an interactive session, where you can input prompts and receive GPT model responses.

## Special commands

These commands are available in both CLI and interactive modes:

- `/private ...`: Disable local logs for the current session.
- `/wrap <number> ...`: Wrap answers to a fixed width.
- `/file <path> <prompt>`: Prepend file contents to prompt.
- `/review YYYY-MM-DD`: Review logs for that date.
- `/setup`: Redefine stored API key.
- `/help`: Print help to console.
- `/exit` or `/quit`: End the current session.

Notes:

- Commands should be entered at the prompt beginning with `/` or `--`.
- The `wrap` command does not affect logs, only console output.
- Alternatively, you can press `Ctrl+D` to end the current session.
- Alternatively, the `private`, `wrap`, and `file` commands can be combined in this exact order. For example:
  - `gpt --wrap 80 --file article.txt summarize`
  - `gpt --private --wrap 100 tell me a joke`

## Installation

Before you can use GPT-TUI, you'll need to install `fry` (>2.5.0), the FatScript interpreter. You can follow these steps to install both:

1. Visit the [official FatScript website](https://fatscript.org) for detailed instructions on how to install `fry` for your operating system, or see bellow the Docker alternative.

2. Once you have `fry` installed, clone the `aprates/gpt-tui` repository from GitLab:

   ```
   git clone https://gitlab.com/aprates/gpt-tui.git
   ```

3. Navigate to the `gpt-tui` directory:

   ```
   cd gpt-tui
   ```

4. Run the following command to create the GPT-TUI executable:

   ```
   fry -b $HOME/.local/bin/gpt gpt.fat
   ```

   > on MacOS use `sudo fry -b /usr/local/bin/gpt gpt.fat` instead

That's it! You should now be able to run the `gpt` command to start using the GPT-TUI application.

## Run with Docker (alternative)

You can also run GPT-TUI on a [docker image](https://hub.docker.com/r/fatscript/fry/tags) without the need to install `fry` on your system.

1. Clone the `aprates/gpt-tui` repository from GitLab:

   ```
   git clone https://gitlab.com/aprates/gpt-tui.git
   ```

2. Navigate to the `gpt-tui` directory:

   ```
   cd gpt-tui
   ```

3. Run `fry` container with current folder as `app` and execute `gpt.fat`:

   ```
   docker run --rm -it -v "$(pwd):/app" fatscript/fry gpt.fat
   ```

You may also map a folder to `/root/.config/gpt-tui` to be able to store the API key and logs.

### Create aliases on your shell configuration file

To simplify running GPT-TUI with Docker, add the provided aliases to your shell's configuration file (e.g., `.bashrc`, `.zshrc`):

```
# gpt (GPT-TUI docker command alias)
[ -d "$HOME/.config/gpt-tui" ] || mkdir -p "$HOME/.config/gpt-tui"
alias gpt='docker run --rm -it -v "/path/to/gpt-tui:/app" -v "$HOME/.config/gpt-tui:/root/.config/gpt-tui" fatscript/fry gpt.fat'
```

Replace `/path/to/gpt-tui` with the actual path to the `gpt-tui` directory on your system.

## Advanced options

For additional configurations, edit the `configs.fat`.

> changes may require you to run `fry -b $HOME/.local/bin/gpt gpt.fat` again

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [GPT-TUI GitLab](https://gitlab.com/aprates/gpt-tui/-/issues).

### Donations

If you find GPT-TUI useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## Credits

GPT-TUI relies on the OpenAI API to generate natural language responses. We acknowledge the OpenAI team for their work on this powerful platform.

## License

[MIT](LICENSE) © 2023-2024 Antonio Prates
